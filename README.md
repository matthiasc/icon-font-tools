Icon font tools
===============

This repository contains some tools for converting symbolic
icons into color fonts that can be used in GTK. It relies heavily
on the nanoemoji tool, which can be found here:

    https://github.com/googlefonts/nanoemoji/

The end goal is to produce fonts containing glyphs in the COLRv1
format, which is compact and supported by both cairo and HarfBuzz.

Symbolic icons
--------------

Symbolic icons are typically produced as SVG images in which
replaceable colors for certain svg elements are indicated by
the style classes "error", "warning" and "success". Elements
which don't have any of these classes are assumed to be rendered
in the foreground color.

Preparing SVG icons
-------------------

The replace-symbolic-colors.py script replaces the fill attribute of
rect, circle and path elements as follows:
- if the element has class "error", set fill to "var(--color0, XYZ)"
- if the element has class "warning", set fill to "var(--color1, XYZ)"
- if the element has class "success", set fill to "var(--color2, XYZ)"
where XYX is the original fill color of the element.
Otherwise, set fill to "currentColor".

This change is necessary, because the OT-SVG spec declares that
`--colorN` variables are the supported way for SVG glyphs to access
colors from the CPAL color palettes in the font, and `currentColor`
is the way to refer to the foreground color.

The script will rename the generated svgs as necessary to ensure
unique names.

Generating a font
-----------------

After running replace-symbolic-colors.py on the symbolic SVGs,
they can be passed to the nanoemoji tool to generate a font.
You probably want to install nanoemoji in a virtualenv, and
you have to set `$PYTHONPATH` so that python finds the
generate-glyphmap module in this repository:

```
export PYTHONPATH=`pwd`:$PYHTONPATH
nanoemoji --glyphmap_generator generate-glyphmap \
          --color_format cff2_colr_1 \
          --keep_glyph_names \
          --family Adwaita \
          --output_file adwaita.ttf \
          FILES
```

This should eventually produce `build/adwaita.ttf` containing
all your SVG icons  converted into the COLRv1 format, with the symbolic colors extracted into a CPAL palette.

Generating a single-icon font
-----------------------------

Sometimes it is more practical to have one file per icon.
This can be achieved by subsetting the icon font:

```
hb-subset --glyphs=GLYPHNAME \
          --glyphnames \
          --output-file=GLYPHNAME.ttf \
          build/adwaita.ttf
```

where GLYPHNAME is the glyph name of the icon you want
to extract. To find the glyph names of your icon, you
can use hb-info:

```
hb-info --list-glyphs adwaita.ttf
```

Note that names ending in decimals are generally components
of composite glyphs.

The output of the hb-subset call should be a font file that
has your desired icon as glyph #1 (glyph #0 is always the
.notdef glyph).

When using a font as a icon, GTK defaults to using glyph 1.
