#! /usr/bin/python3

# Takes an svg as typically used for symbolic icons in GNOME,
# and replaces the fill attributes on rect, circle and path
# elements with constructs that work in OT-SVG fonts. We use
# the first 3 palette entries for error, warning and success
# colors:
#
# - if class is "error", replace fill="X" with fill="var(--color0, X)"
# - if class is "warning", replace fill="X" with fill="var(--color1, X)"
# - if class is "success", replace fill="X" with fill="var(--color2, X)"
# - if no class, set fill="currentColor"

import os
import argparse
from xml.etree import ElementTree as ET

def rewrite(fi, out):
    ET.register_namespace('', "http://www.w3.org/2000/svg")

    with open(fi, 'r') as f:
      tree = ET.parse(f)

    root = tree.getroot()

    namespaces = { '': 'http://www.w3.org/2000/svg',
                   'svg': 'http://www.w3.org/2000/svg' }
    colors = { 'error' : 'color0',
               'warning' : 'color1',
               'success' : 'color2' }

    for elem in root.findall (".//svg:*", namespaces):
        if elem.tag in [ "{http://www.w3.org/2000/svg}rect",
                         "{http://www.w3.org/2000/svg}circle",
                         "{http://www.w3.org/2000/svg}path"]:
            if 'class' in elem.attrib:
                if elem.attrib['class'] in colors:
                    color = colors[elem.attrib['class']]
                    fallback = elem.attrib['fill']
                    elem.attrib['fill'] = 'var(--' + color + ',' + fallback + ')'
                del elem.attrib['class']
            else:
                elem.attrib['fill'] = 'currentColor'

    ET.indent(root, space='    ')

    outfile = os.path.join(out, os.path.basename(fi))
    i = 1
    (base, ext) = os.path.splitext(outfile)
    while os.path.isfile(outfile):
        outfile = "{0}{1}{2}".format(base, i, ext)
        i = i + 1

    output_file = open(outfile, 'w')
    output_file.write(ET.tostring(root, encoding='unicode'))
    output_file.close()

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Process symbolic svg files')
    parser.add_argument('files', metavar='FILE', nargs='+',
                        help='The files to process')
    parser.add_argument('-o', '--out', dest='out', default='.',
                        help='Where to store resulting files')

    args = parser.parse_args()

    os.makedirs (args.out, mode=0o777, exist_ok=True)

    for file in args.files:
        rewrite (file, args.out)
