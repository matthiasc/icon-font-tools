#! /usr/bin/python3

# Takes an a list of svg filenames, and creates an Font.glyphmap
# file that nanoemoji expects to find the glyphnames in.
#
# Glyph names are derived from symbolic icon names by:
# - taking the basename
# - remove the -symbolic.svg suffix
# - replace - by _

import os
from absl import app
from absl import flags
from nanoemoji import util

FLAGS = flags.FLAGS

flags.DEFINE_string("output_file", "-", "Output filename ('-' means stdout)")

def main(argv):
    input_files = util.expand_ninja_response_files(argv[1:])
    with util.file_printer(FLAGS.output_file) as print:
        for file in input_files:
            basename = os.path.basename(file)
            (glyphname, ext) = os.path.splitext(basename)
            glyphname = glyphname.removesuffix('-symbolic')
            glyphname = glyphname.replace('-', '_')
            print(f'{file},,{glyphname},')


if __name__ == "__main__":
    app.run(main)
